import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { User } from './models/user.model';
import { UserService } from './user.service';

@Resolver(() => User)
export class UsersResolver {
  constructor(private userService: UserService) {}

  @Query(() => [User])
  async getUsers(): Promise<User[]> {
    return this.userService.users({});
  }

  @Query(() => User)
  async getUser(): Promise<User> {
    return this.userService.user({});
  }

  // @Mutation(() => User)
  // async createUser(
  //   @Args('data', { type: () => UserCreateInput }) data: UserCreateInput,
  // ): Promise<User> {
  //   return this.userService.createUser(data);
  // }

  @Mutation(() => User)
  async updateUser(
    @Args('id', { type: () => Int }) id: number,
    @Args('name', { nullable: true, type: () => String }) name: string,
    @Args('phone', { nullable: true, type: () => String }) phone: string,
  ): Promise<User> {
    return this.userService.updateUser({
      where: { id: id },
      data: { name: name, phone: phone },
    });
  }
}
